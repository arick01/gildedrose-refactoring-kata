class GildedRose

  def initialize(items)
    @items = items
  end

  def update_quality()
    @items.each do |item|
      # У «Sulfuras» нет срока хранения и не подвержен ухудшению качества
      # Поэтому сразу переходим к следующему товару
      next if item.name == "Sulfuras, Hand of Ragnaros"

      # Уменьшаем срок хранения товаров
      item.sell_in = item.sell_in - 1

      # Изменяем качество товара
      item.quality = case item.name
                       when "Aged Brie"
                         item.quality + 1
                       when "Backstage passes to a TAFKAL80ETC concert"
                         if item.sell_in <= 0
                           0
                         elsif item.sell_in <= 5
                           item.quality + 3
                         elsif item.sell_in <= 10
                           item.quality + 2
                         else
                           item.quality + 1
                         end
                       when "Conjured Mana Cake"
                         item.quality - ((item.sell_in <= 0) ? 4 : 2)
                       else
                         item.quality - ((item.sell_in <= 0) ? 2 : 1)
                     end

      # Качество товара никогда не может быть отрицательным и не может быть больше, чем 50
      if item.quality < 0
        item.quality = 0
      elsif item.quality > 50
        item.quality = 50
      end
    end
  end
end

class Item
  attr_accessor :name, :sell_in, :quality

  def initialize(name, sell_in, quality)
    @name = name
    @sell_in = sell_in
    @quality = quality
  end

  def to_s()
    "#{@name}, #{@sell_in}, #{@quality}"
  end
end
