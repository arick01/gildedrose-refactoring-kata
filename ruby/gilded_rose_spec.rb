require File.join(File.dirname(__FILE__), 'gilded_rose')

describe GildedRose do

  describe "#update_quality" do
    it "does not change the name" do
      items = [
          Item.new("+5 Dexterity Vest", 10, 20),
          Item.new("Aged Brie", 2, 0),
          Item.new("Elixir of the Mongoose", 5, 7),
          Item.new("Sulfuras, Hand of Ragnaros", 0, 80),
          Item.new("Sulfuras, Hand of Ragnaros", -1, 80),
          Item.new("Backstage passes to a TAFKAL80ETC concert", 15, 20),
          Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 49),
          Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 49),
          Item.new("Conjured Mana Cake", 3, 6),
      ]

      days = 30

      gilded_rose = GildedRose.new items
      (0...days).each do |day|
        before_update_items = items.map(&:dup) # Сохрание списока товаров до обновления
        gilded_rose.update_quality
        items.each_with_index do |item, index|
          if item.name == "Sulfuras, Hand of Ragnaros"
            expect(item.quality).to eq 80
            expect(item.sell_in).to eq before_update_items[index].sell_in
          else
            # Вычисление ожидаемого качества товара после обновления
            expected_quality = case item.name
                                 when "Aged Brie"
                                   before_update_items[index].quality + 1
                                 when "Backstage passes to a TAFKAL80ETC concert"
                                   if item.sell_in <= 0
                                     0
                                   elsif item.sell_in <= 5
                                     before_update_items[index].quality + 3
                                   elsif item.sell_in <= 10
                                     before_update_items[index].quality + 2
                                   else
                                     before_update_items[index].quality + 1
                                   end
                                 when "Conjured Mana Cake"
                                   before_update_items[index].quality - ((item.sell_in <= 0) ? 4 : 2)
                                 else
                                   before_update_items[index].quality - ((item.sell_in <= 0) ? 2 : 1)
                               end
            if expected_quality < 0
              expected_quality = 0
            elsif expected_quality > 50
              expected_quality = 50
            end

            # Проверка на соответствие ожидаемому результату
            expect(item.quality).to eq expected_quality
            expect(item.quality).to be >= 0
            expect(item.quality).to be <= 50
          end
        end
      end
    end
  end
end
